import xbmcplugin,xbmcgui
import urllib,urllib2
import httplib
import tempfile
import os
from xml.dom import minidom
from urlparse import urlparse
import urlparse
import sha
import random

#greek iptv
appname = "xbmcgreekiptv"
baseurl = "https://www.greekiptv.com"
host = "www.greekiptv.com"
relativeurl = "/DeskTopModules/DnnIptv/Services/restPlayerService.svc"
serviceurl = baseurl + relativeurl
tempfileprefix = 'tmpgreekiptvimg'
tempfilecount=0
userid=""
password=""
iconImage=""
#string value of login xml
loginstring=""
totalchannelcount = 0

#application_id = xbmc.getInfoLabel('Network.MacAddress')
application_id = ""
application_name = "XBMC greek iptv plugin"
application_version = "0.6"
device_type = "XBMC"

print "----------------------------------------------------------------"
print xbmc.getInfoLabel('system.cpufrequency')
print "----------------------------------------------------------------"

#download web image to local temp file and returns temporary file name
def getWebImage(imgurl):
    global tempfilecount
    tmpfilename = tempfileprefix + str(tempfilecount)
    tmpfilenamewithpath = ((tempfile.gettempdir() + "\\")+tmpfilename)
    # xbmc wants full path for file open
    tfile = file(tmpfilenamewithpath, 'wb')
    tempfilecount=tempfilecount + 1
    i = urllib.urlopen(imgurl)
    tfile.write(i.read())
    tfile.close()    
    return tfile.name


#very primitive method to set given very first tag's value to given one
def setXmlValue(root, tag, value):
    xmlvalue = minidom.Text()
    xmlvalue.nodeValue = value
    xmltag = (root.getElementsByTagName(tag))[0]
    xmltag.appendChild(xmlvalue)



#another primitive method to get value of given tag
def getXmlValue (root, tag):
    xmltag = (root.getElementsByTagName(tag))[0]
    tagvalue = xmltag.firstChild
    return tagvalue.nodeValue
    
# construct login xml
def setloginform ():
    global loginstring, xml
    global userid, password
    response = ""
    try:
        c = httplib.HTTPSConnection(host)
        c.putrequest('GET',(relativeurl + "/login"))
        c.endheaders()
        response = c.getresponse()
    except:
        dialog = xbmcgui.Dialog()
        dialog.ok ("Login Service Not Available","")
        return False
    print response.status, response.reason
    data = response.read()
    print data
    try:
        xml = minidom.parseString(data)
    except:
        dialog = xbmcgui.Dialog()
        dialog.ok ("Login Service Not Available","")
        return False
    
    root = xml.firstChild
    setXmlValue (root, "Login", userid)
    setXmlValue (root, "Password", password)
    setXmlValue (root, "ApplicationID", application_id)
    setXmlValue (root, "ApplicationName", application_name)
    setXmlValue (root, "ApplicationVersion", application_version)
    setXmlValue (root, "DeviceType", device_type)
    
    loginstring = root.toxml()
    
    return True

def showchannels ():
    global totalchannelcount
    headers = {"Content-type": "text/xml", "Content-length": str(len(loginstring)),
               "Accept-Encoding":"*", "User-Agent":"Python"}
    
    conn = httplib.HTTPSConnection(host)
    conn.request("POST", "/DeskTopModules/DnnIptv/Services/restPlayerService.svc/channel", loginstring, headers)
    response = conn.getresponse()
    print response.status, response.reason
    data = response.read()
    conn.close()
    
    xml = minidom.parseString(data)
    root = xml.firstChild
    totalchannelcount = len(root.childNodes)
    print "total number of channel : " + str(totalchannelcount)
    for channelroot in root.childNodes:
        title = getXmlValue(channelroot, "Title")
        channelid = getXmlValue(channelroot, "ChannelID")
        imageurl = getXmlValue(channelroot, "ImageUrl")
        addDir(title,channelid,1,imageurl)

    

#set user id/pass if it does not exist
def loginsetting():
    global userid
    global password
    global application_id

    #get userid and password from user setting
    userid = xbmcplugin.getSetting("userid")
    password = xbmcplugin.getSetting('password')
    password = xbmcplugin.getSetting('application_id')

    if (userid=="") :
        # Read login via a keyboard
        keyboard = xbmc.Keyboard("", heading = "Please type your user id")
        keyboard.setHeading('User id') # optional
        keyboard.doModal()
        if (keyboard.isConfirmed()):
            inputText = keyboard.getText()
            print"User ID: %s"%inputText
    
            # set login
            userid = inputText
    
            dialogInfo = xbmcgui.Dialog()
            dialogInfo.ok("User ID", "Your User ID is:", "%s"%inputText)
            xbmcplugin.setSetting (id='userid', value=userid)
    
        del keyboard

    if (password==""):
        # Read Password via a keyboard
        keyboard = xbmc.Keyboard("", heading = "Please type your password", hidden = True)
        keyboard.setHeading('Password') # optional
        keyboard.setHiddenInput(True) # optional
        keyboard.doModal()
        if (keyboard.isConfirmed()):
            inputText = keyboard.getText()
    
            # set password parameter
            password = inputText
    
            dialogInfo = xbmcgui.Dialog()
            dialogInfo.ok("Password", "Your new password have been set")
            xbmcplugin.setSetting (id='password', value=password)
        keyboard.setHiddenInput(False) # optional
        del keyboard
    
    if (application_id==""):
        random.seed()
        s = sha.new(userid+xbmc.getInfoLabel('Network.MacAddress')+str(random.randint(1,1000000)))
        application_id = s.hexdigest()
        xbmcplugin.setSetting (id='application_id', value=application_id)
        print 'new app id : ' + application_id



def testlogin():
    global loginstring, relativeurl
    #user agent setting is must to set in python post request
    headers = {"Content-type": "text/xml", "Content-length": str(len(loginstring)),
               "Accept-Encoding":"*", "User-Agent":"Python"}
    
    conn = httplib.HTTPSConnection(host)
    
    requesturl = relativeurl + "/login"
    print requesturl
    print loginstring

    
    conn.request("POST", requesturl, loginstring, headers)
    response = conn.getresponse()
    print response.status, response.reason
    if (response.status == 200):
        return True
    elif (response.status == 403):
        resetpassword()
    else:
        dialog = xbmcgui.Dialog()
        dialog.ok("Login Problem",str(response.status)+" "+response.reason)
        return False

def resetpassword():
    return True
    print "reset userid and password"
    xbmcplugin.setSetting (id='userid', value="")
    xbmcplugin.setSetting (id='password', value="")
    dialogInfo = xbmcgui.Dialog()
    dialogInfo.ok("Please Restart Plugin",
                  "to complete setting new userid/password")

# list all avaiable channels
def CATEGORIES():
    global serviceurl
    loginsetting()
    if (setloginform()):
        if (testlogin()):
            showchannels()
            addDir("Reset Password","",2,None)
            xbmcplugin.endOfDirectory(int(sys.argv[1]))
        else:
            xbmcplugin.setSetting (id='userid', value="")
            xbmcplugin.setSetting (id='password', value="")
            dialog = xbmcgui.Dialog()
            dialog.ok ("Invalid userid/password",
                       "userid/password reseted",
                       "please restart plugin")
    
#get new token of give channel id (url)
def PLAY(url):
    global loginstring, iconImage, relativeurl
    print loginstring

##    setloginform()
##    if testlogin()==False:
##        return False
    #user agent setting is must to set in python post request
    headers = {"Content-type": "text/xml", "Content-length": str(len(loginstring)),
               "Accept-Encoding":"*", "User-Agent":"Python"}
    conn = httplib.HTTPSConnection(host)
    
    requesturl = relativeurl + "/channel/" + url.strip() + "/play/false"
    print requesturl
    
    conn.request("POST", requesturl, loginstring, headers)
    response = conn.getresponse()
    print response.status, response.reason
    if (response.status == 200):
        data = response.read()
        conn.close()
    
        print data
        xml = minidom.parseString(data)
        root = xml.firstChild
        urlxml = root.firstChild
        urltoken = urlxml.nodeValue
        urltoken = str(urltoken)
        uparse = urlparse.urlparse(urltoken)
        for a in uparse:
            print a
        tempurl = "mms://" + uparse[1] + uparse[2] + "?" + uparse[4]
        print tempurl
        urltoken = str(tempurl)
        xbmc.Player(xbmc.PLAYER_CORE_MPLAYER).play(urltoken.strip())
    
    else:
        dialog = xbmcgui.Dialog()
        dialog.ok("Error Loading Video",str(response.status)+" "+response.reason)


def get_params():
       param=[]
       paramstring=sys.argv[2]
       if len(paramstring)>=2:
               params=sys.argv[2]
               cleanedparams=params.replace('?','')
               if (params[len(params)-1]=='/'):
                       params=params[0:len(params)-2]
               pairsofparams=cleanedparams.split('&')
               param={}
               for i in range(len(pairsofparams)):
                       splitparams={}
                       splitparams=pairsofparams[i].split('=')
                       if (len(splitparams))==2:
                               param[splitparams[0]]=splitparams[1]

       return param






# due to token requirement for greekiptv, all channels are added as directory
def addDir(name,url,mode,iconimage):
    global loginstring
    ok=True
    if (iconimage == None):
        tempiconimage = 'DefaultVideo.png'
    else:
        tempiconimage = getWebImage(iconimage)
    u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&loginstring="+urllib.quote_plus(loginstring)+"&iconimage="+urllib.quote_plus(tempiconimage)
    xbmc.log(u)
    liz=xbmcgui.ListItem(name, iconImage=tempiconimage, thumbnailImage=tempiconimage)
    liz.setInfo( type="Video", infoLabels={ "Title": name } )
    ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=False,totalItems=int(totalchannelcount+1))
    return ok



params=get_params()
url=None
name=None
mode=None
loginstring=None
iconImage="DefaultVideo.png"

# this url stores channel id for this plug in
try:
       url=urllib.unquote_plus(params["url"])
except:
       pass
try:
       name=urllib.unquote_plus(params["name"])
except:
       pass
try:
       mode=int(params["mode"])
except:
       pass

try:
       loginstring=urllib.unquote_plus(params["loginstring"])
except:
       pass
try:
       iconImage=urllib.unquote_plus(params["iconimage"])
except:
       pass


print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "loginstring: "+str(loginstring)
print sys.argv

if (mode==None or url==None or len(url)<1) and int(sys.argv[1])>=0:
    print "---- greek iptv channel list mode ----"
    CATEGORIES()

elif mode==1:
    print "---- greek iptv channel play mode ----"
    print ""+url
    PLAY(url)
    print "---- end of greek iptv channel play mode ----"

elif mode==2:
    resetpassword()

else:
    setloginform()
    
