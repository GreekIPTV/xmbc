import os
import sys
import urllib
import xbmc
import xbmcgui


class Navigation():
    def __init__(self):
        self.xbmc = sys.modules["__main__"].xbmc
        self.xbmcgui = sys.modules["__main__"].xbmcgui
        self.xbmcplugin = sys.modules["__main__"].xbmcplugin

        self.settings = sys.modules["__main__"].settings
        self.service = sys.modules["__main__"].service
        self.settingsItem = {'Title':'Settings'  , 'Logo':self.service.getPluginImage('settings'), 'action':"settings" }

    def showSettings(self):
        old_user_name = self.service.userName()
        old_user_password = self.service.userPassword()
        self.settings.openSettings()

        user_name = self.service.userName()
        user_password = self.service.userPassword()

        result = ""
        status = 500

        if not user_name:
            return (result, 200)

        self.xbmc.executebuiltin("Container.Refresh")
        return (result, status)

    def MainMenu(self):
        self.CategoryMenu()
        self.addListItem(self.settingsItem)
        self.xbmcplugin.endOfDirectory(handle=int(sys.argv[1]), succeeded=True, cacheToDisc=False)

    def subMenu(self, params={}):
        get = params.get

        if (not get("action")):
            self.addListItem(self.settingsItem)    
        else:
            if (get("action") == 'category'):
                self.ChannelMenu(get("id"))
                self.xbmcplugin.endOfDirectory(handle=int(sys.argv[1]), succeeded=True, cacheToDisc=False)
            elif (get("action") == 'channel'):
                self.StartPlayer(params)
            elif (get("action") == 'settings'):
                self.showSettings()
            else:
                print plugin + " ARGV Nothing done.. verify params " + repr(params)

    def CategoryMenu(self):
        if(not self.service.userName() == "" and not self.service.userPassword() == "" ):
            categories = self.service.getCategories()
            if(categories):
                for cat in categories:
                    self.addListItem(cat)

    def ChannelMenu(self, categoryID = ''):
        if(not self.service.userName() == "" and not self.service.userPassword() == "" ):
            channels = self.service.getChannels(categoryID)
            if(channels):
                for ch in channels:
                    self.addListItem(ch)

    def StartPlayer(self, params={}):
        get = params.get

        icon = self.service.getImageByChannel(get('id'))
        thumbnail = icon

        listitem = self.xbmcgui.ListItem(get("Title"), iconImage=icon, thumbnailImage=thumbnail)

        url = self.service.getMediaUrl(get('id'))

        listitem.setProperty("Video", "true")
        listitem.setInfo(type='Video', infoLabels=params)
        
        xbmc.Player().play(item = url, listitem = listitem)

    def addListItem(self, params={}):
        item = params.get
        folder = True

        icon = item("Logo", self.service.getPluginImage('explore'))
        thumbnail = item("Logo", self.service.getPluginImage('explore'))

        listitem = self.xbmcgui.ListItem(item("Title"), iconImage=icon, thumbnailImage=thumbnail)

        url = '%s?path=%s&' % (sys.argv[0], item("path"))
        url += 'action=' + item("action") + '&'
        url += 'Title=' + item("Title") + '&'
        if (item("action") == "category"):
            url += 'id=' + item("CategoryID") + '&'
        if (item("action") == "channel"):
            url += 'id=' + item("ChannelID") + '&'
        if (item("Logo")):
            url += 'Logo=' + item("Logo") + '&'

        self.xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]), url=url, listitem=listitem, isFolder=folder)
