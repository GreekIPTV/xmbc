import sys
import xbmc
import xbmcgui
import xbmcaddon
import xbmcplugin
import CommonFunctions as common
from uuid import uuid4


# xbmc hooks
settings = xbmcaddon.Addon(id='plugin.video.greekiptv')


if (__name__ == "__main__" ):
    import GreekIptvService
    service = GreekIptvService.Service()
    import GreekIptvNavigation
    navigation = GreekIptvNavigation.Navigation()


    if (not settings.getSetting("firstrun")):
        navigation.showSettings()
        settings.setSetting("firstrun", "1")

    if (not settings.getSetting("appId")):
        navigation.showSettings()
        settings.setSetting("appId", str(uuid4()))

    if (not sys.argv[2]):
        navigation.MainMenu()
    else:
        params = common.getParameters(sys.argv[2])
        navigation.subMenu(params)
       
